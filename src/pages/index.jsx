import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import MissionSearch from '../components/organisms/MissionSearch'
const Home = () => {
  return (
    <React.Fragment>
      {/* <CssBaseline /> */}
      <Container maxWidth="md">
        {/* <Typography component="div" style={{ backgroundColor: '#cfe8fc', height: '100vh' }} /> */}
        <Grid container spacing={3}>
        <Grid item xs>
        <MissionSearch />
   
        </Grid>
     
        </Grid>

       
      </Container>
    </React.Fragment>
  );
}



export default Home
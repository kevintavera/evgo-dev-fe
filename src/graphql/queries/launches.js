import gql from 'graphql-tag'

export default gql`
query {
  launches {
    mission_id
    mission_name
    details
    flight_number
  	launch_year
    launch_date_unix
    rocket {
      rocket_name
      
    }
    links {
      video_link
      youtube_id
      mission_patch_small
    }
  }
}
`

// export default gql`
// query { 
//   cities {
//   id
//   name
//   }
// }
// `

// `
//   query Countries {
//     countries {
//       name
//       code
//     }
//   }
// `
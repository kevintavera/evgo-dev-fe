import React, { useState } from 'react'
import { useQuery } from '@apollo/react-hooks'
import LAUNCHES_QUERY from '../../../graphql/queries/launches'
import Launch from '../../atoms/Launch'
import Grid from '@material-ui/core/Grid';
const MissionSearch = () => {
  const { loading, error, data } = useQuery(LAUNCHES_QUERY)
  const [selectedLaunch, setSelectedLaunch] = useState('')
  if (error) {
    return (
      <div data-testid="error-div" className="launch-selector">
        Error loading launches!
      </div>
    )
  }
  if (loading) {
    return (
      <div data-testid="loading-div" className="launch-selector">
        Loading Launches...
      </div>
    )
  }

  const { launches = [] } = data
  let filterByRocket = [...new Set(launches.map(l => l.rocket.rocket_name))];
  let filterByYear = [...new Set(launches.map(l => l.launch_year))];
  let filterByMissionName = [...new Set(launches.map(l => l.mission_name))]
  return (
    <div data-testid="selector-container" className="launch-selector">
      <div>Choose a Mission</div>
      <select
        value={selectedLaunch.flight_number}
        onChange={e => setSelectedLaunch(launches.filter(l => l.mission_name == e.target.value))}
      >
        {filterByMissionName.map(mission => (
          <option
            key={mission}
            value={mission}
          >
            {mission}
          </option>
        ))}
      </select>
      <div>Choose a Rocket Name</div>
      <select
        value={selectedLaunch.flight_number}
        onChange={e => setSelectedLaunch(launches.filter(l => l.rocket.rocket_name == e.target.value))}
      >
        {filterByRocket.map(rocket => (
          <option
            key={rocket}
            value={rocket}
          >
            {rocket}
          </option>
        ))}
      </select>
      <div>Choose a Launch Year</div>
      <select
        value={selectedLaunch.launch_year}
        onChange={e => setSelectedLaunch(launches.filter(l => l.launch_year == e.target.value))}
      >
        {filterByYear.map(year => (
          <option
            key={year}
            value={year}
          >
            {year}
          </option>
        ))}
      </select>
<div data-testid="chosen-text">

       
       {selectedLaunch ? 

   <Launch launches={selectedLaunch} />
       
       : 'No launch selected...'
      }
      </div>
     </div>
   
  )
}

export default MissionSearch

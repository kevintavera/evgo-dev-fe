import react, {useState} from 'react'
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import styled from 'styled-components';

const VideoLink = styled.img`
.youtube {
    position: relative;
    display: inline-block;
}
.youtube:before {
    content: "";
    position: absolute;
    width: 100%;
    height: 100%;
    z-index: 10;
    background: transparent url({play icon url here}) center center no-repeat;
}
`
const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'left',
      color: theme.palette.text.secondary,
    },
  }));
const Launch = function(props) {
    const {launches} = props;
    const classes = useStyles();
    return (

        launches.map(launch => 

<Paper className={classes.paper}>
<Grid container spacing={3}>
  
    <Grid item justify="center" >
    <h1><img style={{height: '4rem'}} src={launch.links.mission_patch_small} alt="test test" /> {launch.mission_name}</h1>
    <div>{new Date(launch.launch_date_unix*1000).toString()}</div>
    <h3>Rocket: {launch.rocket.rocket_name}</h3>
 
    <div>
        <i>
            {launch.details}
        </i>
    </div>
    <hr />
    <div style={{position: 'inline'}}>
    <a class="youtube" href={`http://youtube.com/watch?v=${launch.links.youtube_id}`} target="_blank">
    <img src={`http://i3.ytimg.com/vi/${launch.links.youtube_id}/0.jpg`} alt="{ video title here }" />
    </a>
    </div>
   


    
         {/* <iframe width="560" height="315" src={`https://www.youtube.com/embed/${launch.links.youtube_id}`} title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> */}
    </Grid>

</Grid>
   
         
         
        
</Paper>

  
)

        )
       
    
}
export default Launch